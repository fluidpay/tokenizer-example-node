var express = require("express");
var app = express();
var bodyParser = require("body-parser");
var path = require("path");
var axios = require("axios");

app.set('view engine', 'ejs');

app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());

app.get('/', function (req, res) {
    res.render('form')
});

app.post('/submit', function (req, res) {
    var token = req.body.token;
    var po_number = req.body.po_number;

    var apiRequest = {
        type: "sale",
        amount: 100,
        payment_method: {
            token: token
        }
    }

    var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'api_privatekeygoeshere'
    }
    instance = axios.create({
        headers: headers
    });

    instance.post("https://sandbox.fluidpay.com/api/transaction", apiRequest)

        .then((response) => {
            if (response.data.status === "success") {
                console.log(response.data.data.response_body)
                res.render('success', {
                    id: response.data.data.id,
                    status: response.data.data.status
                })
            } else {
                console.log(response)
                res.render('error', {
                    message: response.data.msg
                })
            }

        })
        .catch((error) => {
            console.log(error)
            res.render('error', {
                message: error.data.msg
            })
        })
});

app.listen(8080);

console.log("Running at Port 8080");
